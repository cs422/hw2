#include "pin.H"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <limits.h>

/* ================================================================== */
// Global variables 
/* ================================================================== */
#define taken 1
#define notTaken 0
#define SAg 0
#define Gag 1
#define SG 0
#define Gg 1
#define gS 2
#define resultMaxLen 9
#define btbMaxLen 5
#define condBrCount 0
#define staticMispredCount 1
#define bimodalMispredCount 2
#define sagMispredCount 3
#define gagMispredCount 4
#define gshareMispredCount 5
#define metaSGMispredCount 6
#define metaMajorityMispredCount 7
#define meta3MispredCount 8 
#define indirectCallsCount 0
#define btb1MispredCount 1
#define btb2MispredCount 3
#define btb1MissCount 2
#define btb2MissCount 4
#define phtHeight 512
#define bhtHeight 1024
#define btbHeight 128
#define btbWidth 4
#define btbIndexBits 7


uint bimodalPHT[phtHeight];
uint sagBHT[bhtHeight];
uint sagPHT[phtHeight];
uint GHR;
uint gagPHT[phtHeight];
uint gsharePHT[phtHeight];
uint metaSG[phtHeight];
uint meta3PHT[3][phtHeight];

UINT64 resultForArray[resultMaxLen];
UINT64 resultBackArray[resultMaxLen];
UINT64 btbResultArray[btbMaxLen];

/*
1. NOPs             (XED_CATEGORY_NOP)
2. Direct calls         (XED_CATEGORY_CALL)
3. Indirect calls       (XED_CATEGORY_CALL)
4. Returns                  (XED_CATEGORY_RET)
5. Unconditional branches   (XED_CATEGORY_UNCOND_BR)
6. Conditional branches     (XED_CATEGORY_COND_BR)
7. Logical operations       (XED_CATEGORY_LOGICAL)
8. Rotate and shift         (XED_CATEGORY_ROTATE | XED_CATEGORY_SHIFT)
9. Flag operations      (XED_CATEGORY_FLAGOP)
10. Vector instructions     (XED_CATEGORY_AVX | XED_CATEGORY_AVX2 | XED_CATEGORY_AVX2GATHER)
11. Conditional moves       (XED_CATEGORY_CMOV)
12. MMX and SSE instructions    (XED_CATEGORY_MMX | XED_CATEGORY_SSE)
13. System calls        (XED_CATEGORY_SYSCALL)
14. Floating-point      (XED_CATEGORY_X87_ALU)
15. Others: whatever is left
*/

UINT64 icount = 0;
std::ostream * out = &cerr;
UINT64 fast_forward = 0;
struct BTB {
	bool isValid;
	ADDRINT target;
	int rank;
	ADDRINT tag;
};

int lruHead1[btbHeight];
int lruHead2[btbHeight];
BTB **BTB1;
BTB **BTB2;


KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  "pintool",
    "o", "", "specify file name for output");

KNOB<UINT32> KnobAmount(KNOB_MODE_WRITEONCE,  "pintool",
    "f", "0", "specify fast-forward amount in billions");

INT32 Usage()
{
    cerr << "This tool prints out the number of dynamically executed " << endl <<
            "instructions, basic blocks and threads in the application." << endl << endl;

    cerr << KNOB_BASE::StringKnobSummary() << endl;

    return -1;
}

VOID InsCount()  {
    icount++;
}

ADDRINT Terminate(void)
{
        return (icount >= (fast_forward + 1000000000));
}

// Analysis routine to check fast-forward condition
ADDRINT FastForward(void) {
    return (icount >= fast_forward && icount < (fast_forward + 1000000000));
}


// Analysis routine to exit the application
VOID MyExitRoutine() {
	uint i;
	double forCount = (double)(resultForArray[condBrCount]);
	double backCount = (double)(resultBackArray[condBrCount]);
	double condBrTotalCount = forCount + backCount;
	char tab = '\t';
	char sep[20] = "===================";
	*out << sep << "Part A" << sep << endl;
	char label [8][3] = {"A","B","C","D","E","F","G1","G2"};
	*out << "Label\tOverall\t\tForward\t\tBackward\n";
	*out << "Total\t" << std::fixed << std::setprecision(0) <<  condBrTotalCount << tab << std::fixed << forCount << tab << std::fixed << backCount << endl;
	for( i=1;i<resultMaxLen;i++){
		*out << label[i-1] << tab << std::fixed << std::setprecision(6) << ((double)(resultForArray[i])+(double)(resultBackArray[i]))/condBrTotalCount << tab << (double)(resultForArray[i])/forCount << tab << (double)(resultBackArray[i])/backCount << endl;
	}
	*out << endl << sep << "Part B" << sep << endl;
	*out << "\nLabel\tMispredictions\tMisses\n";
	char btbChar[6][50] = {"Total Indirect Calls are: ","BTB1","dfsf", "BTB2"};
	for(i=1;i<btbMaxLen;i+=2){
		*out << btbChar[i] << tab << std::fixed << std::setprecision(6) << (double)(btbResultArray[i])/(double)(btbResultArray[0]) << tab << (double)(btbResultArray[i+1])/(double)(btbResultArray[0]) << endl;
	}
	*out << btbChar[0] << tab << btbResultArray[0] << endl;
        exit(0);
}

VOID calcBTB_Results(ADDRINT currAddr,ADDRINT jumpAddr,ADDRINT nextAddr){
	btbResultArray[indirectCallsCount]++;
	int counterMiss,i,index,j=0,counterMispred,hitRank=-1;
	int *lruHead;
	bool miss = 1;
	BTB **btbTemp;
	ADDRINT predTarget = 0,tag = 0;
	
 	//BTB1
	index = currAddr & 0x7F;
	btbTemp = BTB1;
	counterMispred = btb1MispredCount;
	counterMiss = btb1MissCount;
	lruHead = lruHead1;
	tag = currAddr >> 7;

	for(int k=0;k<2;k++){
		
		for(i=0;i<btbWidth;i++){
			if(btbTemp[index][i].isValid && tag == btbTemp[index][i].tag){
				predTarget = btbTemp[index][i].target;
				hitRank = btbTemp[index][i].rank;
				miss = 0;
				break;
			}
		}
		if(!miss && lruHead[index] != i ){
			for(j=0;j<btbWidth;j++){
				if(btbTemp[index][j].isValid && btbTemp[index][j].rank < hitRank)
					btbTemp[index][j].rank++;
			}
			btbTemp[index][i].rank = 0;
			lruHead[index] = i;
		}

		if(!miss && predTarget != jumpAddr){
			btbResultArray[counterMispred]++;
			btbTemp[index][i].target = jumpAddr;
		}

		if(miss){
			btbResultArray[counterMiss]++;
			if(jumpAddr != nextAddr)
				btbResultArray[counterMispred]++;
			for(i=0;i<btbWidth;i++){
				if(!btbTemp[index][i].isValid)
					break;
				if(btbTemp[index][i].rank == 3)
					j = i;
			}
			if(i != btbWidth){
				j = i;
			}
			for(i=0;i<btbWidth;i++){
				if(btbTemp[index][i].isValid && j != i)
					btbTemp[index][i].rank++;
			}
			btbTemp[index][j].isValid = 1;
			btbTemp[index][j].tag = tag;
			btbTemp[index][j].rank = 0;
			btbTemp[index][j].target = jumpAddr;
		}

		//BTB2
		index = (currAddr ^ GHR ) & 127;
		btbTemp = BTB2;
		counterMispred = btb2MispredCount;
		counterMiss = btb2MissCount;
		lruHead = lruHead2;
		tag = currAddr;
		miss = 1;
	}
}

VOID calcDP_Results(ADDRINT currAddr,ADDRINT jumpAddr, BOOL reality){
	bool bimodalPrediction,Sprediction,Gprediction,gprediction,prediction;
	uint temp,temp2;
	int index; 
	UINT64 *resultArray;
	if(currAddr < jumpAddr){  //Forward Branch
		if(reality == taken)
			resultForArray[staticMispredCount]++;
		resultArray = resultForArray;
	}
	else{
		if(reality == notTaken)
			resultBackArray[staticMispredCount]++;
		resultArray = resultBackArray;
	}
	resultArray[condBrCount]++;
	//bimodal
	index = currAddr%phtHeight;
	temp = bimodalPHT[index];
	bimodalPrediction = temp >> 1;
	if(reality != bimodalPrediction)
		resultArray[bimodalMispredCount]++;
	if(reality == taken && temp != 3)
		bimodalPHT[index]++;
	else if(reality == notTaken && temp != 0)
		bimodalPHT[index]--;
	//SAg	
	index = currAddr%bhtHeight;
	temp = sagBHT[index]%phtHeight;
	temp2 = sagPHT[temp];
	Sprediction = temp2 >> 1;
	if(reality != Sprediction)
		resultArray[sagMispredCount]++;
	if(reality == taken && temp2 != 3)
		sagPHT[temp]++;
	else if(reality == notTaken && temp2 != 0)
		sagPHT[temp]--;
	sagBHT[index] = (temp << 1 ) + reality;
	//GAg
	index = GHR%phtHeight;
	temp = gagPHT[index];
	Gprediction = temp >> 2;
	if(reality != Gprediction)
		resultArray[gagMispredCount]++;
	if(reality == taken && temp != 7)
		gagPHT[index]++;
	else if(reality == notTaken && temp != 0)
		gagPHT[index]--;
	// gshare
	index = (GHR ^ currAddr) % phtHeight;
	temp = gsharePHT[index];
	gprediction = temp >> 2;
	if(reality != gprediction)
		resultArray[gshareMispredCount]++;
	if(reality == taken && temp != 7)
		gsharePHT[index]++;
	else if(reality == notTaken && temp != 0)
		gsharePHT[index]--;
	//Hybrid of sag and gag
	index = GHR%phtHeight;
	temp = metaSG[index];
	if(Sprediction != Gprediction){
		if(temp >> 1)
			prediction = Gprediction;
		else 
			prediction = Sprediction;
		if(reality == Gprediction && temp != 3)
			metaSG[index]++;
		else if(reality == Sprediction && temp != 0)
			metaSG[index]--;
	}
	else
		prediction = Sprediction;
	if(reality != prediction)
		resultArray[metaSGMispredCount]++;
	//Hybrid of all three with majority meta predictor
	if(Gprediction == Sprediction)
		prediction = Gprediction;
	else if(gprediction == Sprediction)
		prediction = gprediction;
	else
		prediction = Gprediction;
	if(reality != prediction)
		resultArray[metaMajorityMispredCount]++;
	//Hybrid of all 3, complex meta
	index = GHR%phtHeight;
	if(meta3PHT[SG][index] >> 1){
		if(meta3PHT[Gg][index] >> 1)
			prediction = gprediction;
		else
			prediction = Gprediction;
	}
	else{
		if(meta3PHT[gS][index] >> 1)
			prediction = Sprediction;
		else
			prediction = gprediction;
	}
	if(Sprediction != Gprediction){
		temp = meta3PHT[SG][index];
		if(reality == Gprediction && temp != 3)
			meta3PHT[SG][index]++;
		else if(reality == Sprediction && temp != 0)
			meta3PHT[SG][index]--;
	}
	if(Sprediction != gprediction){
		temp = meta3PHT[gS][index];
		if(reality == Sprediction && temp != 3)
			meta3PHT[gS][index]++;
		else if(reality == gprediction && temp != 0)
			meta3PHT[gS][index]--;
	}
	if(Gprediction != gprediction){
		temp = meta3PHT[Gg][index];
		if(reality == gprediction && temp != 3)
			meta3PHT[Gg][index]++;
		else if(reality == Gprediction && temp != 0)
			meta3PHT[Gg][index]--;
	}
	if(reality != prediction)
		resultArray[meta3MispredCount]++;
	//updating GHR 
	GHR = (GHR << 1) + reality;
}

VOID Trace(TRACE trace, VOID *v)
{
	// Visit every basic block in the trace
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		for(INS ins = BBL_InsHead(bbl);  INS_Valid(ins); ins = INS_Next(ins)){
			INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)Terminate, IARG_END);
			INS_InsertThenCall(ins, IPOINT_BEFORE, (AFUNPTR)MyExitRoutine,  IARG_END);           
			//Part A -- DP
			if(INS_Category(ins) == XED_CATEGORY_COND_BR){
				INS_InsertIfCall(ins,IPOINT_BEFORE,(AFUNPTR)FastForward,IARG_END);
				INS_InsertThenPredicatedCall(ins,IPOINT_BEFORE,(AFUNPTR)calcDP_Results,IARG_ADDRINT,INS_Address(ins),IARG_BRANCH_TARGET_ADDR,IARG_BRANCH_TAKEN,IARG_END);
			}
			//Part B -- BTB
			if(INS_Category(ins) == XED_CATEGORY_CALL && !INS_IsDirectCall(ins)){
				INS_InsertIfCall(ins,IPOINT_BEFORE,(AFUNPTR)FastForward,IARG_END);
				INS_InsertThenPredicatedCall(ins,IPOINT_BEFORE,(AFUNPTR)calcBTB_Results,IARG_ADDRINT,INS_Address(ins),IARG_BRANCH_TARGET_ADDR,IARG_ADDRINT,INS_NextAddress(ins),IARG_END);
			}
			//common to all
			INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)InsCount, IARG_END);                  
		}
	}
}

VOID Fini(INT32 code, VOID *v)
{
	*out <<  "===============================================" << endl;
	*out <<  "MyPinTool analysis results: " << endl;
	*out <<  "Number of instructions: " << icount  << endl;
	*out << "fast_forward count is: " << fast_forward << endl;
	*out <<  "===============================================" << endl;
	MyExitRoutine();
}


int main(int argc, char *argv[])
{
	// Initialize PIN library. Print help message if -h(elp) is specified
	// in the command line or the command line is invalid 
	if( PIN_Init(argc,argv) )
	{
		return Usage();
	}
	//common
	UINT32 j = 0,i = 0;
	fast_forward = (UINT64)(KnobAmount.Value())*1000000000; 
	string fileName = KnobOutputFile.Value();
	if (!fileName.empty()) { out = new std::ofstream(fileName.c_str());}

	for(i=0;i<resultMaxLen;i++){
		resultForArray[i] = 0;
		resultBackArray[i] = 0;
	}
	GHR = 0;
	for(i = 0;i<phtHeight;i++)
		bimodalPHT[i] = sagPHT[i] = gagPHT[i] = gsharePHT[i] = metaSG[i] = 0;
	for(i=0;i<3;i++){
		for( j =0;j<phtHeight;j++)
			meta3PHT[i][j] = 0;
	}
	for(i = 0;i<bhtHeight;i++)
		sagBHT[i] = 0;

	BTB1 = (BTB **)malloc(btbHeight*sizeof(BTB *));
	BTB2 = (BTB **)malloc(btbHeight*sizeof(BTB *));

	for(i=0;i<btbHeight;i++){
		*(BTB1+i) = (BTB*)malloc(btbWidth*sizeof(BTB));
		*(BTB2+i) = (BTB*)malloc(btbWidth*sizeof(BTB));
	}

	for(i=0;i<btbHeight;i++){
		for(j=0;j<btbWidth;j++){
			BTB1[i][j].isValid = BTB2[i][j].isValid = 0;
			BTB1[i][j].tag = BTB2[i][j].tag = 0;
			BTB1[i][j].target = BTB2[i][j].target  = 0;
			BTB1[i][j].rank = BTB2[i][j].rank = 4;
		}
	}
	for(i=0;i<btbMaxLen;i++)
		btbResultArray[i] = 0;
	// Register function to be called to instrument traces
	TRACE_AddInstrumentFunction(Trace, 0);

	// Register function to be called when the application exits
	PIN_AddFiniFunction(Fini, 0);

	cerr <<  "===============================================" << endl;
	cerr <<  "This application is instrumented by MyPinTool" << endl;
	if (!KnobOutputFile.Value().empty()) 
	{
		cerr << "See file " << KnobOutputFile.Value() << " for analysis results" << endl;
	}
	cerr << "value of fast-forward count is: " << fast_forward << endl;
	cerr <<  "===============================================" << endl;

	// Start the program, never returns
	PIN_StartProgram();

	return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
